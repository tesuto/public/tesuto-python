from tesuto.apis import Emulation


class TestEmulation(object):
    def test_list(self):
        resp = Emulation.list(missing=True)
        assert resp.status_code == 200
        assert isinstance(resp.data, list)
        assert isinstance(resp.data[0], Emulation)
        assert isinstance(resp.data[0].id, int)
        assert isinstance(resp.data[0].slug, str)
        assert isinstance(resp.data[0].region_id, int)
        assert isinstance(resp.data[0].is_ztp, bool)

    def test_id(self):
        resp = Emulation.get(12345, missing=True)
        assert resp.status_code == 404
        resp = Emulation.list(missing=True)
        resp = Emulation.get(resp.data[0].id, missing=True)
        assert resp.status_code == 200
        assert isinstance(resp.data, Emulation)
        assert isinstance(resp.data.is_ztp, bool)
        assert isinstance(resp.data.devices[0].id, int)
        assert isinstance(resp.data.devices[0].interfaces, list)
        assert isinstance(resp.data.topologies, list)
