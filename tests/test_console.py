import sys
import json

from contextlib import contextmanager
from io import StringIO

from tesuto.core.console import Console


@contextmanager
def reader(stdout: bool = True, stderr: bool = True):
    # Use stringio to store output
    if stdout:
        out = StringIO()
    if stderr:
        err = StringIO()

    sys_out, sys_err = sys.stdout, sys.stderr

    try:
        # Use StringIO stdout, stderr
        if stdout:
            sys.stdout = out
        if stderr:
            sys.stderr = err
        yield sys.stdout, sys.stderr
    finally:
        # Reset stdout and stderr back to sys
        sys.stdout, sys.stderr = sys_out, sys_err


class TestConsole(object):

    def test_json(self):
        console = Console()
        with reader() as (stdout, stderr):
            console.to_json({'tests': []})
        output = json.loads(stdout.getvalue().strip())
        assert 'tests' in output

    def test_list(self):
        console = Console()
        with reader() as (stdout, stderr):
            console.to_json(['a', 'b', 'c'])
        output = json.loads(stdout.getvalue().strip())
        assert output['type'] == 'array'
        assert isinstance(output['data'], list)

    def test_string(self):
        console = Console()
        with reader(stderr=False) as (stdout, stderr):
            console.to_json('test')
        output = json.loads(stdout.getvalue().strip())
        assert output['type'] == 'string'
        assert output['data'] == 'test'

    def test_display(self):
        console = Console()
        with reader() as (stdout, stderr):
            console.display('test')
        assert 'test' in stdout.getvalue().strip()

    def test_dict(self):
        console = Console()

        class Test(object):
            a = 1

            def __init__(self):
                self.b = 2

        test = Test()
        with reader() as (stdout, stderr):
            console.to_json(test)

        output = json.loads(stdout.getvalue().strip())
        print(output)
        assert output['a'] == 1
        assert output['b'] == 2

    def test_csv(self):
        console = Console()
        with reader() as (stdout, stderr):
            console.to_csv({'tests': ['a', 'b', 'c']})
        assert 'tests' in stdout.getvalue()
