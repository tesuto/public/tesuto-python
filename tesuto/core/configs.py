"""Configs.

Config module will be set from constants/environment,
or a loaded ini file which will us the environment var TESUTO_CONFIG.
This module is basically an extention of the configparser module with namespaces.

Key search order:
    - An ini config section
    - Constant (which can also have environment variables.
    - The default ini config section
    - If not key is found then return the default value if any.
"""
import os
import configparser

from collections.abc import MutableMapping

from . import const


class EnvInterpolation(configparser.BasicInterpolation):
    """Interpolation which expands environment variables in values."""

    def before_get(self, parser, section, option, value, defaults):
        if value:
            return os.path.expandvars(value)


class Config(MutableMapping):
    config = None
    namespaces = {}

    def __new__(cls, *args, **kwargs):
        if not cls.config:
            cls.config = object.__new__(Config)
        return cls.config

    def __init__(self):
        self.load_config()

        # Add constants to defaults config, overwritten by any inifile configs.
        for key in const.CONSTANTS:
            self.setdefault(key, getattr(const, key, None), section='default', namespace='default')

    def load_config(self, ini_file: str = None, namespace: str = 'default'):
        ini_file = ini_file or const.TESUTO_CONFIG
        if not self.has_namespace(namespace):
            self.namespaces[namespace] = self.load_ini(ini_file)
        else:
            raise KeyError("{} namespace already exists.".format(namespace))

    def load_ini(self, inifile):
        ini = configparser.ConfigParser(interpolation=EnvInterpolation())
        if inifile and os.path.exists(inifile):
            ini.read(inifile)
        return ini

    def set(self, key, value, section: str = 'default', namespace: str = 'default'):
        # Cast values to string as that is the only valid type for configparser.
        section = section or 'default'
        if not self.has_section(section, namespace):
            self.add_section(section, namespace)
        self.get_namespace(namespace).set(section, key, str(value))

    def setdefault(self, key, value, section: str = 'default', namespace: str = 'default'):
        # Create an initial default if the key doesn't exist
        if self.has_option(key, section, namespace):
            return False
        else:
            self.set(key, value, section, namespace)
            return True

    def has_option(self, key, section: str = 'default', namespace: str = 'default'):
        n = self.get_namespace(namespace)
        return n.has_option(section, key)

    def get_namespace(self, namespace: str = 'default'):
        return self.namespaces[namespace]

    def has_namespace(self, namespace: str):
        return namespace in self.namespaces

    def with_namespace_section(self, namespace: str = 'default', section: str = 'default'):
        if self.has_section(section, namespace):
            return self.get_namespace(namespace)[section]
        else:
            raise KeyError("Namespace or section error {}:{}".format(namespace, section))

    def defaults(self, namespace: str = 'default'):
        for k, v in self.get_namespace(namespace).items('default'):
            yield k

    def __setitem__(self, key, value):
        return self.set(key, value, section='default', namespace='default')

    def __getitem__(self, key, default: str = None):
        return self.get_namespace('default').get('default', key, fallback=default)

    def __delitem__(self, key):
        return self.get_namespace('default').delete(key)

    def __len__(self):
        return len(self.get_namespace('default')['default'])

    def __iter__(self):
        return iter(self.get_namespace('default')['default'])

    def items(self, section: str = 'default', namespace: str = 'default'):
        for item in self.get_namespace(namespace).items(section=section):
            yield item

    def sections(self, namespace: str = 'default'):
        for section in self.get_namespace(namespace).sections:
            yield section

    def has_section(self, section: str = None, namespace: str = 'default'):
        return self.get_namespace(namespace).has_section(section)

    def add_section(self, section: str = None, namespace: str = 'default'):
        if not self.has_section(section, namespace=namespace):
            self.get_namespace(namespace).add_section(section)

    def remove_section(self, section: str = None, namespace: str = 'default'):
        return self.get_namespace(namespace).remove_section(section)

    def delete(self, key: str, section: str = 'default', namespace: str = 'default'):
        if self.has_section(section):
            return self.get_namespace(namespace).remove_option(section, key)

    def get(self, key: str, default: str = None,
            section: str = 'default', namespace: str = 'default'):
        n = self.with_namespace_section(namespace, section)
        return n.get(key, fallback=default)

    def getboolean(self, key: str, default: bool = None,
                   section: str = 'default', namespace: str = 'default'):
        n = self.with_namespace_section(namespace, section)
        return n.getboolean(key, fallback=default)

    def getint(self, key: str, default: int = None,
               section: str = 'default', namespace: str = 'default'):
        n = self.with_namespace_section(namespace, section)
        return n.getint(key, fallback=default)

    def getfloat(self, key: str, default: float = None,
                 section: str = 'default', namespace: str = 'default'):
        n = self.with_namespace_section(namespace, section)
        return n.getfloat(key, fallback=default)


config = Config()
