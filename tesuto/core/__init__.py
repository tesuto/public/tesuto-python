# Tesuto Python bindings
# API docs at http://api-docs.tesuto.com
# Authors:
# Tesuto Dev <dev@tesuto.com>

# Set default logging handler to avoid "No handler found" warnings.
import logging
from logging import NullHandler

logging.getLogger(__name__).addHandler(NullHandler())
