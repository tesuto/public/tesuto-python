from tesuto.models import BaseModel


class Notification(BaseModel):

    class Options(BaseModel.Options):
        __resource__ = 'notifications'
        __mapper__ = 'data'
