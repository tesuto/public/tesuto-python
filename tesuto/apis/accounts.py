from tesuto.models import BaseModel, types


class Organization(BaseModel):
    name = types.StringType()
    slug = types.StringType()
    active = types.BooleanType()

    class Options(BaseModel.Options):
        __resource__ = 'organizations'
        __mapper__ = ['organizations', 'organization']


class User(BaseModel):
    name = types.StringType()
    active = types.BooleanType()
    accepted_terms = types.BooleanType(default=True)
    active = types.BooleanType()
    login_count = types.IntType()
    last_login_at = types.DateTimeType()
    last_login_ip = types.StringType()
    current_login_ip = types.StringType()
    current_login_at = types.DateTimeType()
    confirmed_at = types.DateTimeType()
    email = types.StringType()

    class Options(BaseModel.Options):
        __resource__ = 'users'
        __mapper__ = 'users'

    @types.serializable
    def tokens(self):
        pass

    @types.serializable
    def roles(self):
        pass

    @types.serializable
    def organizations(self):
        pass


class Token(BaseModel):
    token = types.StringType()
    expires = types.DateTimeType()
    is_active = types.BooleanType()
    organization_id = types.StringType()

    class Options(BaseModel.Options):
        __resource__ = 'tokens'
        __mapper__ = 'data'

    @types.serializable
    def roles(self):
        pass
