"""Tesuto

Base module used to import other tesuto specific libraries.
"""
from tesuto.core import helpers, errors
from tesuto.core.logger import FileLogger
from tesuto.core.commands import Command
from tesuto.core.console import Console
from tesuto.core.configs import config
from tesuto import apis

# init a console for quick data dumping
console = Console()

__all__ = [
    'apis',
    'errors',
    'FileLogger',
    'helpers',
    'config',
    'console',
    'Console',
    'Command',
]
